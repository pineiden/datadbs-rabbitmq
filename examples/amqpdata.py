from data_amqp import AMQPData
# monitoring
import asyncio
from tasktools.taskloop import TaskLoop
from rich import print

async def show(queue, **kwargs):
    if not queue.empty():
        for i in range(queue.qsize()):
            item = await queue.get()
            print(item)
    return (queue,), kwargs
    
if __name__=='__main__':
    opts=dict(
        source=True,
        amqp=True,
        code='status',
        host = "10.54.217.95",
        credentials = ('enugeojson_prod', 'geognss_prod'),
        vhost = "gpsdata",
        exchange='status_exchange',
        queue_name = "status_gnss",
        durable=True,
        routing_key='status',
        consumer_tag='status_data'        
        )

    amqp = AMQPData(**opts)
    #amqp.connect()

    queue = asyncio.Queue()
    args = (queue, True)
    task = TaskLoop(amqp.consume_exchange_mq, coro_args=args, coro_kwargs={})
    task.create()
    
    task = TaskLoop(show, coro_args=(queue,), coro_kwargs={})
    task.create()

    loop = asyncio.get_event_loop()
    loop.run_forever()
