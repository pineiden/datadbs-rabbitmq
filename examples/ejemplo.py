import pika
from pika.credentials import PlainCredentials
from pika.connection import ConnectionParameters
from pika.adapters.blocking_connection import BlockingConnection


# monitoring

host = "10.54.217.95"
creds = ('enugeojson_prod', 'geognss_prod')
vhost = "gpsdata"
queue_name = "gpsstream"

def connect(creds, host,vhost, queue_name, durable=True):
	credentials = PlainCredentials(*creds)
	parameters = ConnectionParameters(host = host,
                                          virtual_host = vhost,
                                          credentials = credentials)
	connection = BlockingConnection(parameters)
	channel = connection.channel()
	if durable:
		channel.queue_declare(queue_name, durable=durable)
	else:
		channel.queue_declare(queue_name)
	return channel
		
		
channel=connect(creds, host, vhost, queue_name, durable=False)
print(channel)
